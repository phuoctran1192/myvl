// $(function(){
//     var helper = new Helper();
//     helper.initMenu();
//     helper.fullpage();
//     helper.bannerslide();
//     helper.itemslide('.newproduct-slider',4,4,3,2,1);
//     helper.itemslide('.toy-slider',4,4,3,2,1);
//     helper.itemslide('.news-slider',4,2,2,1,1);
//     helper.itemslide('.sameitemlist',5,4,3,2,1);
    
// 	helper.totop();
// 	helper.showsearch();
// 	helper.itemslick();
// 	helper.showlogopt();
// 	helper.fixtop();
// 	helper.showcart();
// 	helper.showtooltip();
// });


// function Helper(){
//     var methods = this;
//     methods.initMenu = function(){
// 		var open = false;
// 		$('#btn-toggle').on('click' , function(){
// 			if( open){
// 				$('#menu').removeClass('open');
// 				$('#btn-toggle').removeClass('active');
// 				$('body').removeClass('menu-open');
// 			}else{
// 				$('#menu').addClass('open');
// 				$('#btn-toggle').addClass('active');
// 				$('body').addClass('menu-open');
// 			}
// 			open = !open;
// 		});
// 		$('body, html').on('click', function(event){
// 			var target = $(event.target);
// 			if( !target.is('#menu , #menu * ,#btn-toggle ,#btn-toggle *')){
// 				$('#menu').removeClass('open');
// 				$('#btn-toggle').removeClass('active');
// 				$('body').removeClass('menu-open');
// 				open = false;
// 			}
// 		});
// 		$('#menu .has-submenu .icon-first-submenu').on('click' , function(event){
// 			event.preventDefault();
// 			var parent = $(this).parents('.has-submenu');
// 			$('#menu .has-submenu').not(parent).removeClass('open').find('.submenu').slideUp()
// 			$('#menu .has-submenu').not(parent).find('.has-second-submenu').removeClass('open');
// 			$('#menu .has-submenu').not(parent).find('.second-submenu').slideUp();
// 			parent.toggleClass('open').find('.submenu').slideToggle();
// 		});
// 		$('#menu .has-second-submenu .icon-second-submenu').on('click' , function(event){
// 			event.preventDefault();
// 			var parent = $(this).parents('.has-second-submenu');
// 			$('#menu .has-second-submenu').not(parent).removeClass('open').find('.second-submenu').slideUp();
// 			parent.toggleClass('open').find('.second-submenu').slideToggle();
// 		});
// 	}
// 	methods.fullpage = function(){
// 		var fheight = $(window).height()-($('header').outerHeight()+ $('footer').outerHeight());
// 		$('.wrapper').css('min-height',fheight);
// 	}
//     methods.bannerslide = function(){
//         var owl = $('.bannerslider');
// 			owl.owlCarousel({
// 				items:3,
// 				loop:true,
// 				nav:true,
// 				navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
// 				margin:15,
// 				autoplay:true,
// 				autoplayTimeout:3000,
// 				autoplayHoverPause:true,
// 				responsive:{
// 				320:{
// 					items:1,
// 				},
// 				600:{
// 					items:1,
// 				},
// 				768:{
// 					items:1,
// 				},
// 				1024:{
// 					items:1,
// 				}
// 			}
// 			});
//     }
//     methods.partnerslide = function(){
//         var owl1 = $('.listpartner');
// 			owl1.owlCarousel({
// 				items:5,
// 				loop:true,
// 				nav:false,
// 				navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
// 				margin:30,
// 				autoplay:true,
// 				autoplayTimeout:3000,
// 				autoplayHoverPause:true,
// 				responsive:{
// 				320:{
// 					items:2,
// 				},
// 				600:{
// 					items:3,
// 				},
// 				768:{
// 					items:5,
// 				},
// 				1024:{
// 					items:8,
// 				}
// 			}
// 			});
//     }
//     methods.itemslide = function(el,n1,n2,n3,n4,n5){
//         var owl = $(el);
// 			owl.owlCarousel({
// 				items:n1,
// 				loop:true,
// 				nav:true,
// 				navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
// 				margin:25,
// 				autoplay:false,
// 				autoplayTimeout:3000,
// 				autoplayHoverPause:true,
// 				responsive:{
// 				320:{
// 					items:n5,
// 				},
// 				600:{
// 					items:n4,
// 				},
// 				992:{
// 					items:n3,
// 				},
// 				1200:{
// 					items:n2,
// 				}
// 			}
// 			});
// 	}
// 	methods.totop = function(){
// 		$(window).bind('scroll', function () {
// 			if ($(window).scrollTop() > 200) {
// 				$('.totop').css({"opacity":"1","visibility":"visible","transform":"translateY(0%)"});
// 			} else {
// 				$('.totop').css({"opacity":"0","visibility":"hidden","transform":"translateY(100%)"});
// 			}
// 		});
// 		$('.totop').on('click', function(event) {
// 		event.preventDefault();
// 		/* Act on the event */
// 		$('html,body').animate({scrollTop: 0}, 400);
// 		});
// 	}
//     methods.showsearch = function(el){
//         $('#open-search').click(function() {
// 			$('#headsearch').toggleClass("ssearch");
// 		});
// 		$('body, html').on('click', function(event){
// 			var target = $(event.target);
// 			if( !target.is('#open-search , #open-search *,.headsearch,.headsearch *')){
// 				$('#headsearch').removeClass('ssearch');
// 			}
// 		});
//     }
//     methods.showlogopt = function(){
//         var curr = $("#cont1");
// 		$(".logopts input[type='radio']").on('change', function() {
// 			curr = $("input[type='radio']:checked").val();
// 			$(curr).siblings().removeClass("showlogopt");
// 			$(curr).addClass("showlogopt")
// 		});
//     }
   
// 	methods.fixtop = function(){
// 		$(window).bind('scroll', function () {
// 			if ($(window).scrollTop() > 50) {
// 				$('body').addClass('fixed');
// 			} else {
// 				$('body').removeClass('fixed');
// 			}
// 		});
// 	}

// 	methods.itemslick = function(){
// 		$('.thumblist').slick({
// 			dots: false,
// 			nextArrow: '<i class="fa fa-angle-right"></i>',
// 			prevArrow: '<i class="fa fa-angle-left"></i>',
// 			slidesToShow: 3,
// 			slidesToScroll: 1,
// 			centerMode: true,
// 			  focusOnSelect: true,
// 			asNavFor: '.big-photo'
// 		});
// 		$('.big-photo').slick({
// 			dots: false,
// 			arrows: false,
// 			mobileFirst:true,
// 			fade:true,
// 			slidesToShow: 1,
// 			slidesToScroll: 1,
// 			verticalSwiping: false,
// 			asNavFor: '.thumblist'
// 		});
// 		$('#modalqview').on('shown.bs.modal', function(){
// 			$('#modalqview').find('.slick-slider').slick('setPosition',0);
// 		})
// 	}
// 	methods.showcart = function (){
// 		$('#open-cart').on('click', function(e){
// 			e.preventDefault();
// 			$('#cart').addClass('active');
// 			$('body').addClass('open-cart');
// 		})
// 		$('.cart .btn-close').on('click', function(){
// 			$('#cart').removeClass('active');
// 			$('body').removeClass('open-cart');
// 		})
// 		$('body, html').on('click', function(event){
// 			var target = $(event.target);
// 			if( !target.is('#cart , #cart * ,#open-cart,#open-cart *')){
// 				$('#cart').removeClass('active');
// 				$('body').removeClass('open-cart');
// 			}
// 		});
// 	}
// 	methods.showtooltip = function () {
// 		$(document).ready(function(){
//             $('[data-toggle="tooltip"]').tooltip();   
//         });
// 	  }

	



   


//     return methods;
// }
// JavaScript Document

(function($){
	var $window = window,
		$timeout = setTimeout;
		
	window.Snow = function(element, settings) {
		this.settings = settings, 
		this.flakes = [], 
		this.flakeCount = settings.count, 
		this.mx = -100, 
		this.my = -100,
		function() {
			var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || function(callback) {
				$timeout(callback, 1e3 / 60)
			};
			$window.requestAnimationFrame = requestAnimationFrame
		}(), 
		this.init(element)
	};
	Snow.prototype.init = function(element) {
		this.canvas = element.get(0), this.ctx = this.canvas.getContext("2d"), this.canvas.width = $window.innerWidth, this.canvas.height = $window.innerHeight, this.flakes = [];
		for (var i = 0; i < this.flakeCount; i++) {
			var x = Math.floor(Math.random() * this.canvas.width),
				y = Math.floor(Math.random() * this.canvas.height),
				size = Math.floor(100 * Math.random()) % this.settings.size + 2,
				speed = Math.floor(100 * Math.random()) % this.settings.speed + Math.random() * size / 10 + .5,
				opacity = .5 * Math.random() + this.settings.opacity;
			this.flakes.push({
				speed: speed,
				velY: speed,
				velX: 0,
				x: x,
				y: y,
				size: size,
				stepSize: Math.random() / 30,
				step: 0,
				angle: 180,
				opacity: opacity
			})
		}
		1 == this.settings.interaction && this.canvas.addEventListener("mousemove", function(e) {
			this.mx = e.clientX, this.my = e.client
		});
		var thiz = this;
		$($window).resize(function() {
			thiz.ctx.clearRect(0, 0, thiz.canvas.width, thiz.canvas.height), thiz.canvas.width = $window.innerWidth, thiz.canvas.height = $window.innerHeight
		});
		if(typeof this.settings.image === "string") {
			this.image = $("<img src='resources/images/" + this.settings.image + "' style='display: none'>");
		};
		
		this.snow();
	}, Snow.prototype.snow = function() {
		var thiz = this,
			render = function() {
				thiz.ctx.clearRect(0, 0, thiz.canvas.width, thiz.canvas.height);
				for (var i = 0; i < thiz.flakeCount; i++) {
					var flake = thiz.flakes[i],
						x = thiz.mx,
						y = thiz.my,
						minDist = 100,
						x2 = flake.x,
						y2 = flake.y,
						dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y));
					if (minDist > dist) {
						var force = minDist / (dist * dist),
							xcomp = (x - x2) / dist,
							ycomp = (y - y2) / dist,
							deltaV = force / 2;
						flake.velX -= deltaV * xcomp, flake.velY -= deltaV * ycomp
					} else
						switch (flake.velX *= .98, flake.velY <= flake.speed && (flake.velY = flake.speed), thiz.settings.windPower) {
							case !1:
								flake.velX += Math.cos(flake.step += .05) * flake.stepSize;
								break;
							case 0:
								flake.velX += Math.cos(flake.step += .05) * flake.stepSize;
								break;
							default:
								flake.velX += .01 + thiz.settings.windPower / 100
						}
					if (flake.y += flake.velY, flake.x += flake.velX, (flake.y >= thiz.canvas.height || flake.y <= 0) && thiz.resetFlake(flake), (flake.x >= thiz.canvas.width || flake.x <= 0) && thiz.resetFlake(flake), 0 == thiz.settings.image) {
						var grd = thiz.ctx.createRadialGradient(flake.x, flake.y, 0, flake.x, flake.y, flake.size - 1);
						grd.addColorStop(0, thiz.settings.startColor), grd.addColorStop(1, thiz.settings.endColor), thiz.ctx.fillStyle = grd, thiz.ctx.beginPath(), thiz.ctx.arc(flake.x, flake.y, flake.size, 0, 2 * Math.PI), thiz.ctx.fill()
					} else
						thiz.ctx.drawImage(thiz.image.get(0), flake.x, flake.y, 2 * flake.size, 2 * flake.size)
				}
				$window.cancelAnimationFrame(render), $window.requestAnimationFrame(render)
			};
		render()
	}, Snow.prototype.resetFlake = function(flake) {
		if (0 == this.settings.windPower || 0 == this.settings.windPower)
			flake.x = Math.floor(Math.random() * this.canvas.width), flake.y = 0;
		else if (this.settings.windPower > 0) {
			var xarray = Array(Math.floor(Math.random() * this.canvas.width), 0),
				yarray = Array(0, Math.floor(Math.random() * this.canvas.height)),
				allarray = Array(xarray, yarray),
				selected_array = allarray[Math.floor(Math.random() * allarray.length)];
			flake.x = selected_array[0], flake.y = selected_array[1]
		} else {
			var xarray = Array(Math.floor(Math.random() * this.canvas.width), 0),
				yarray = Array(this.canvas.width, Math.floor(Math.random() * this.canvas.height)),
				allarray = Array(xarray, yarray),
				selected_array = allarray[Math.floor(Math.random() * allarray.length)];
			flake.x = selected_array[0], flake.y = selected_array[1]
		}
		flake.size = Math.floor(100 * Math.random()) % this.settings.size + 2, flake.speed = Math.floor(100 * Math.random()) % this.settings.speed + Math.random() * flake.size / 10 + .5, flake.velY = flake.speed, flake.velX = 0, flake.opacity = .5 * Math.random() + this.settings.opacity
	};
	
	$.fn.snow = function(){
		$(this).each(function(i, e) {
			var scope = {};
			$.each(e.attributes,function(index, key) {
				scope[ $.camelCase(key.name) ] = Number.isFinite( Number(key.value) ) ? Number(key.value) : key.value
			});
			if ( typeof scope.image === "string" && scope.image === "false" ) {scope.image = false};
			
			new Snow($(e), {
				speed: 1||0,
				interaction: scope.interaction||!0,
				size: scope.size||2,
				count: scope.count||200,
				opacity: scope.opacity||1,
				startColor: scope.startColor||"rgba(255,255,255,1)",
				endColor: scope.endColor||"rgba(255,255,255,0)",
				windPower: scope.windPower||0,
				image: scope.image||!1
			});
		});
	}
})(jQuery);